#! /bin/bash

# Install packages
#apt install build-essential libcgroup-dev python3-hdf5storage libhdf5-dev man2html libfreeipmi-dev libibmad-dev libibumad-dev lua5.4 liblua5.4-dev munge libmunge-dev libpam-slurm libpam0g-dev libnuma-dev numactl numad rrdtool libgtk2.0-dev libgtk-4-dev libjson-c-dev libhttp-parser-dev python3-http-parser libyaml-dev libjwt-dev libopenmpi-dev hwloc jwt perl mariadb-server mariadb-client libmariadb-dev libreadline-dev libcanberra-gtk-module libpmix-bin libpmix-dev libpmix2 libdbus-1-dev dbus nvidia-driver-530 nvidia-utils-530 nvidia-cuda-toolkit -y

# Download SLURM in home directory of root
cd $HOME
git clone https://github.com/SchedMD/slurm.git

# Move to SLURM folder, configure, compile, and install SLURM with required options
cd slurm
./configure --config-cache --prefix=/usr/slurm_vm_23.11 --sysconfdir=/etc/slurm_vm_23.11 --with-http-parser=/usr/ --with-yaml=/usr/ --with-jwt=/usr/ --with-mysql_config=/usr/bin --enable-debug --with-pmix=/usr/lib/x86_64-linux-gnu/pmix2
make -j 6
make install

# Copy linked libraries, and service unit files (according to machine role) to correct locations
ldconfig -n /usr/slurm_vm_23.11/lib
cp etc/slurmctld.service /etc/systemd/system # For SLURM controller machine
cp etc/slurmdbd.service /etc/systemd/system # For SLURM accounting machine
cp etc/slurmd.service /etc/systemd/system # For SLURM compute nodes

# - Modify the /etc/environemtn to add the correct path for SLURM commands (e.g., '/usr/slurm_vm_23.11/sbin' and '/usr/slurm_vm_23.11/bin')
source /etc/environment

# Restart SLURM daemons according to machine role(s)
systemctl restart slurmctld.service # For SLURM controller machine
systemctl restart slurmdbd.service # For SLURM accounting machine
systemctl restart slurmd.service # For SLURM compute nodes

# These steps are for REST API with SLURM. slurmrestd may be needed only on the SLURM controller machine. So, slurmrestd may not need to be installed on compute nodes
# - Uncomment the User and Group attributes to specify an unprivileged user (e.g., slurm_rest_user) to run the slurmrestd daemon (of course, the unprivileged user had to be created first), because it is mentioned in the service unit file that slurmrestd should NOT be run as root, nor slurm users.
# - Modify the line "ExecStart=/usr/slurm_vm_23.11/sbin/slurmrestd $SLURMRESTD_OPTIONS unix:/usr/slurm_vm_23.11/com/slurmrestd.socket 0.0.0.0:6820" to "ExecStart=/usr/slurm_vm_23.11/sbin/slurmrestd $SLURMRESTD_OPTIONS 0.0.0.0:6820"
cp etc/slurmrestd.service /etc/systemd/system # May be needed on all machines to use REST APIs with SLURM after the making the previous modifications
systemctl restart slurmrestd.service

# If Ubuntu 18.04, it is better to use cgroup v1, not 2. So, it is better to discard the following steps
# To enable cgroupv2
##To enable cgroupv2, please have a look at the section “How to convert my nodes to Control Group v2?” [here](https://slurm.schedmd.com/faq.html). If cgroupv2 causes problems when installed on the cluster (with Ubuntu 22.04, which I don\'t know why), then see a similar topic [here](https://stackoverflow.com/questions/70965770/unable-to-mount-memory-cgroup) which disabled the cgroupv2 in the kernel command line by editing GRUB_CMDLINE_LINUX parameter to GRUB_CMDLINE_LINUX=\" cgroup_enable=memory swapaccount=1 systemd.unified_cgroup_hierarchy=0\" in the /etc/default/grub file. Of course, after this change, the command update-grub should be executed.
sed -e 's@^GRUB_CMDLINE_LINUX=@#GRUB_CMDLINE_LINUX=@' -i /etc/default/grub
echo 'GRUB_CMDLINE_LINUX="systemd.unified_cgroup_hierarchy=1 systemd.legacy_systemd_cgroup_controller=0 cgroup_no_v1=all"' >> /etc/default/grub
update-grub
reboot