#!/bin/sh
#SBATCH --time=30:00:00
#SBATCH --nodes=2
#SBATCH --ntasks-per-node=2
#SBATCH --job-name=test_mpi
#SBATCH -p LocalQ
#SBATCH -o mpi_out%j.out
#SBATCH -e mpi_err%j.err

#echo -e '\n submitted Open MPI job'
export PATH=/home/mpich-3.3.1-install/bin:$PATH
export LD_LIBRARY_PATH=/home/mpich-3.3.1-install/lib:$LD_LIBRARY_PATH:
mpirun -np 4 /home/test_user3/NPB3.4.2/NPB3.4-MPI/bin/is.A.x

