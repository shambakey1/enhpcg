#!/bin/sh
#SBATCH --account=all_clusters_account
#SBATCH --time=30

#SBATCH --job-name=test_openmpi
#SBATCH -o openmpi_out%j.out
#SBATCH -e openmpi_err%j.err

echo -e '\n submitted Open MPI job'
echo 'hostname'
hostname

# load Open MPI module
#module load openmpi/gcc

# compile the C file
#mpicc test_mpi.c -o test_mpi

# run compiled test_openmpi.c file
#mpiexec.slurm
#mpiexec.openmpi test_openmpi
#mpirun $HOME/test_openmpi # srun can also be used instead of mpirun as shown in the following step. But, it was mentioned in the openmpi docs that srun does not have all the available options for mpirun. However, it seems mpirun is a little bit slower in submitting the job
srun --mpi=pmix_v4 $HOME/test_openmpi # This step can also be used instead of mpirun
