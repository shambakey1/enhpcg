#!/bin/bash

#echo -e '\n submitted MPI job'
#export PATH=/home/mpich-3.3.1-install/bin:$PATH
#export LD_LIBRARY_PATH=/home/mpich-3.3.1-install/lib:$LD_LIBRARY_PATH:
#mpirun -np 1 /home/test_user3/GridNPB3.1/GridNPB3.1-SHF-SER/bin/sp.S 

max_njobs=144 # Max number of jobs. Minimum is 0
max_rep=1 # Maximum number of repeatition for each experiement.
#res_dir="/home/test_user3/NPB3.3.1/NPB3.3-SER/ed_results/slurm/144serial/" # Path to keep results
res_prefix="ed_slurm_srta_all" # Prefix for output/error files
exp_time=$(date +%s_%N) # Initiation time of current experiments. Used as UUID for resulting files
clusters="asuslurm,slurmcluster2" # List of clusters to submit slurm jobs
mkdir -p $res_dir/$exp_time # Create sub-directory to store results of current experiment
#res_dir_timed=$res_dir/$exp_time # Modify path to store results of current experiment by appending current time

for (( rep=0; rep<$max_rep; rep++ )) # Traverse over required number of repeatitions
do
	# Submit jobs for execution on specified clusters
	echo "Submitting jobs for repitition $rep"
	for (( i=0; i<$max_njobs; i++ ))
	do
		#echo $res_dir/$res_prefix\_$i\_$rep\_$exp_time
		sbatch -n1 -N1 --mem=70mb --job-name=$res_prefix\_$i\_$rep\_$exp_time -A all_clusters_account -M$clusters --export=res_prefix=$res_prefix,i=$i,rep=$rep,exp_time=$exp_time --wait /home/test_user3/serial_base.sh &	# The output and error files specified here may not be very important because the underling 'serial_base.sh' creates the actual output and error files in a shared directory
		#sbatch -n1 -N1 --mem=70mb --job-name=$res_prefix\_$i\_$rep\_$exp_time -A all_clusters_account -M$clusters --export=res_prefix=$res_prefix,i=$i,rep=$rep,exp_time=$exp_time --wait /home/test_user3/serial_base.sh &
	done
	wait

	# Create an array of job names
	#job_names_list=()
	#for (( i=0;  i<$max_njobs; i++ ))
	#do
	#	job_names_list[$i]=$res_prefix\_$i\_$rep\_$exp_time
	#done

	# Collect statistics of jobs in JSON files
    for (( i=0; i<$max_njobs; i++ )) # Traverse over required number of jobs
	do
	#while [[ ${#job_names_list[@]} -gt 0 ]]; do	# Check if statistics about all jobs have already been collected
	#	echo "array lenght ${#job_names_list[@]}"
	#	for i in ${!job_names_list[@]}
	#		do
				echo "Statistics about job: $res_prefix\_$i\_$rep\_$exp_time"
	#			echo "Statistics about job: ${job_names_list[$i]}"
	#			job_status=$(squeue --noheader --name ${job_names_list[$i]} -o "%T")	# Check the status of a specific job by index
	#			if [[ -z "$job_status" ]]; then	# If no output from squeue, then job has already finished
	#				sacct --name=${job_names_list[$i]} -M$clusters --json > $res_dir/$res_prefix\_$i\_$rep\_$exp_time.json	# As job finished, collect its statistics in a JSON file
	#				unset ${job_names_list[$i]}	# Remove the current job form the list of jobs to be checked
	#				break	# Continue to the beginning of the array for another check if the array is not empty
	#			fi

                sacct --name=$res_prefix\_$i\_$rep\_$exp_time -M$clusters --json > $res_dir/$exp_time/$res_prefix\_$i\_$rep\_$exp_time.json
                #sacct --name=$res_prefix\_$i\_$rep\_$exp_time -M$clusters --json >> $res_dir/out.json
#        		done
	done

	# Copy any files that exist on the other cluster to current results directory
	sshpass -p 'Clu$ter#iri' scp -r headnode:NPB3.3.1/NPB3.3-SER/ed_results/slurm/$exp_time $res_dir
done
