#!/bin/sh
#SBATCH --account=all_clusters_account
#SBATCH --time=1
#SBATCH --gres=gpu

#srun hostname |sort
#srun singularity --nv exec library://alpine cat /etc/alpine-release
srun singularity exec --nv ubuntu_jammy.sif ./saxpy
