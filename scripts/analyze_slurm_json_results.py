import json, argparse, os, math, pandas as pd
from glob import glob
from pathlib import Path
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt


def extract_slurm_accounting_res(res_path:str)->dict:
    ''' Extract required information from an input JSON file into a dictionary
    @param res_path: Path to input JSON file containing workload statistics (e.g., slurm sacct output about a specific job in JSON format)
    @type res_path: Str
    @return: Dictionary containing required information
    @rtype: dict
    '''

    # Load input JSON file
    with open(res_path,'r') as f:
        r=json.load(f)

    # Extract required dictionary provided that each job contains one step that contains one task
    d={'name': r['jobs'][0]['name'], \
    'cluster':r['jobs'][0]['cluster'], \
    'node':r['jobs'][0]['nodes'], \
    'elapsed':r['jobs'][0]['time']['elapsed'], \
    'eligible':r['jobs'][0]['time']['eligible'], \
    'end':r['jobs'][0]['time']['end'], \
    'start':r['jobs'][0]['time']['start'], \
    'submission':r['jobs'][0]['time']['submission'], \
    'suspended':r['jobs'][0]['time']['suspended']}

    return d

def extract_npb_results(res_path:str)->dict:
    ''' Extract required information from an NPB file into a dictionary
    @param res_path: Path to input file containing output of NPB
    @type res_path: Str
    @return: Dictionary containing required information
    @rtype: dict
    '''

    r={'name':Path(res_path).stem}    # Final results dictionary
    with open(res_path,'r') as f:
        for l in f.readlines(): # Read input file line by line
            if '=' in l:    # Just focus on the last part of the NPB output file
                l=[i.strip() for i in l.split('=')] # convert the line into a list after removing white spaces (just for formatting)
                if 'Time in seconds' in l[0] or 'Mop/s total' in l[0]:
                    l[1]=float(l[1])
                elif 'Iterations' in l[0]:
                    l[1]=int(l[1])
                r[l[0]]=l[1] # Append the new items

    return r

def md(x,r_inf,n_half):
    ''' Model function of the HPC as number of tasks during a given time
    @param x: Input time intervals
    @type x: List
    @param r_inf: r_infinity
    @type r_inf: float
    @param n_half: n_half
    @type n_half: float
    '''

    return x*r_inf-n_half

def clusterCurveFitting(final_res:str,md_fn)->dict:
    ''' Get parameters of linear model of cluster(s)
    @param final_res: File path containing CSV information about finished jobs
    @type final_res: Str
    @md_fn: Objective linear function to model the cluster(s)
    @return: Dictionary of model parameters and model coveriance
    @rtype: dict
    '''

    f=pd.read_csv(final_res)  # Read input CSV file
    min_st=f['submission'].min()    # Earliest start time of any job submission/eligibility/start
    sorted_ends=(f['end'].sort_values()-min_st).to_list()
    jobs=[i for i in range(1,len(sorted_ends)+1)]
    popt,pcov=curve_fit(md,sorted_ends,jobs)
    return {'r_inf':popt[0],'n_half':popt[1],'m_covarience':pcov}

def sortJobbyTimeLevels(final_res:str,final_res_sorted_time:str='')->None:
    ''' Sort a given CSV file, which contains jobs dates from submission to eligible to start to end
    @param final_res: Path to file to be sorted by times
    @type final_res: str
    @param final_res_sorted_time: Path to save the times sorted file
    @type final_res_sorted_time: str
    '''

    df=pd.read_csv(final_res)
    df.sort_values(['submission','eligible','start','end'])
    df.to_csv(final_res_sorted_time)

def sortJobByName(final_res,final_res_sorted_name:str=""):
    ''' Special function to sort jobs by names. This function depends on a specific convention for file name that ends with "job number"_"first part of timestamp"_"second part of time stamp"
    @param final_res: Path to file to be sorted by names, or pandas dataframe of the final results to be sorted by names
    @type final_res: str
    @param final_res_sorted_name: Path to save the names sorted file
    @type final_res_sorted_name: str
    @return: Name sorted dataframe of jobs finle names
    @rtype: dataframe
    '''

    if isinstance(final_res,str):
        df=pd.read_csv(final_res) # Read the original, non-name sorted, input CSV file 
    else:
        df=final_res
    df_sorted=df[0:0]   # New dataframe to contain the name-sorted jobs
    jobs_numbers=[int(i.split('_')[-4]) for i in df['name']] # List of jobs numbers which reside at the fourth position from the end of the file name
    jobs_numbers.sort()   # Sort jobs numbers assccendingly
    com_file_name_parts=df['name'][0].split('_')    # Parts of each file name seperated by '_' delimiter
    sorted_file_names=["_".join(com_file_name_parts[:-4])+"_"+str(i)+"_"+"_".join(com_file_name_parts[-3:]) for i in range(len(jobs_numbers))] # List of name sorted files corresponding to jobs
    for i in sorted_file_names:
        df_sorted=df_sorted.append(df.loc[df['name']==i])
    if final_res_sorted_name:
        if isinstance(final_res,str):
            sorted_file_names.to_csv(final_res_sorted_name)
        else:
            sorted_file_names.to_csv(final_res_sorted_name,index=False,header=True) # Save name sorted dataframe into a CSV file
    return df_sorted

def sortJobByName2(final_res,final_res_sorted_name:str=""):
    ''' Special function to sort jobs by names. This function depends on a specific convention for file name that ends with "job number part 1"_"job number part 2"_"first part of timestamp"_"second part of time stamp"
    @param final_res: Path to file to be sorted by names, or pandas dataframe of the final results to be sorted by names
    @type final_res: str
    @param final_res_sorted_name: Path to save the names sorted file
    @type final_res_sorted_name: str
    @return: Name sorted dataframe of jobs finle names
    @rtype: dataframe
    '''

    if isinstance(final_res,str):
        df=pd.read_csv(final_res) # Read the original, non-name sorted, input CSV file 
    else:
        df=final_res
    df_sorted=df[0:0]   # New dataframe to contain the name-sorted jobs
    jobs_numbers=[i.split('_')[-4:-2] for i in df['name']] # List of jobs numbers which reside at the fourth position from the end of the file name
    jobs_numbers=[[int(x[1]),int(x[0])] for x in jobs_numbers] 
    jobs_numbers.sort()   # Sort jobs numbers assccendingly
    com_file_name_parts=df['name'][0].split('_')    # Parts of each file name seperated by '_' delimiter
    sorted_file_names=["_".join(com_file_name_parts[:-4])+"_"+str(i[1])+"_"+str(i[0])+"_"+"_".join(com_file_name_parts[-2:]) for i in jobs_numbers] # List of name sorted files corresponding to jobs
    for i in sorted_file_names:
        #df_sorted=df_sorted.append(df.loc[df['name']==i])
        df_sorted=df_sorted.concat(df.loc[df['name']==i])
    if final_res_sorted_name:
        if isinstance(final_res,str):
            df_sorted.to_csv(final_res_sorted_name)
        else:
            df_sorted.to_csv(final_res_sorted_name,index=False,header=True) # Save name sorted dataframe into a CSV file
    return df_sorted

def plotJobsStackedTime(final_res:str,res_image:str,fig_width:int=25,fig_height:int=10,xlabel_rot:float=0,xlabel_size:float=10,final_res_sorted_time:str='')->None:
    ''' Plot image of the stacked times of jobs from submission to eligible to start to end
    @param final_res: File path containing CSV information about finished jobs
    @type final_res: str
    @param final_res_sorted_time: Path to save time sorted file if required. If empty, then it is not required to save this intermediate result
    @type final_res_sorted_time: str
    @param fig_width: Resulting figure width
    @type fig_width: int
    @param fig_height: Resulting figure height
    @type fig_height: int
    @param xlabel_rot: Rotation of xticks. Default is 0
    @type xlabel_rot: float
    @param xlabel_size: Font size of xticks. Defualt is 10
    @type xlabel_size: float
    @res_image: Path to resulting image
    @type res_image: str
    '''

    # Prepare file
    dfs=pd.read_csv(final_res) # Read input final result
    #dfs=df.sort_values(['submission','eligible','start','end']) # Re-arrange the resulting CSV file according to timed submission, eligible, start, end
    min_submission=dfs['submission'].min()  # Minimum submission time to be the base for other timings
    if 'to submission' not in dfs:
        dfs['to submission']=dfs['submission']-min_submission   # Delay time for each job since the first submission
    if 'submissiton to eligible' not in dfs:
        dfs['submission to eligible']=dfs['eligible']-dfs['submission'] # Time to decide eligibility of each job
    if 'eligible to start' not in dfs:
        dfs['eligible to start']=dfs['start']-dfs['eligible']   # Time to start job from eligiblity
    if 'start to end' not in dfs:
        dfs['start to end']=dfs['end']-dfs['start'] # Time to finish job from starting it
    if final_res_sorted_time:   # Save time sorted if required
        dfs.to_csv(final_res_sorted_time)

    # Plot different times (i.e., levels) of each job as stacked bar
    #l=[i for i in dfs.index]
    plt.clf()   # Precaution to clear any existing previously images
    fig=dfs.plot(x='node',y=['to submission','submission to eligible', \
    'eligible to start','start to end'],xlabel='node allocated to job', \
    ylabel='time (sec)',stacked=True,kind='bar',fontsize=8,rot=90,)
    if fig_width:
        fig.figure.set_figwidth(fig_width)
    if fig_height:
        fig.figure.set_figheight(fig_height)
    if res_image:
        fig.figure.savefig(res_image)


    '''
    fig, ax = plt.subplots(figsize=(fig_width, fig_height))
    plt.bar(l,dfs['submission to eligible'],bottom=dfs['submission'],tick_label=dfs['node'],label='submission to eligible',color='red')
    plt.bar(l,dfs['eligible to start'],bottom=dfs['eligible'],tick_label=dfs['node'],label='eligible to start',color='blue')
    plt.bar(l,dfs['start to end'],bottom=dfs['start'],tick_label=dfs['node'],label='start to end',color='green')
    plt.xlabel("Job-node allocation")
    plt.xticks(rotation=90,fontsize=10)
    plt.ylabel("Time (sec)")
    plt.legend(loc='best')
    plt.savefig(res_image)
    '''

def extractSlurmMulticlusterCriteria(log_file:str='')->dict:
    ''' Extract criterion used by Slurm to decide which cluster should receive a job.
    Currently, this includes: 1) start time of each cluster (the earliest start time is assigned the job).
    2) In case of equal start times, then the smaller preemption count cluster is assigned the job.
    3) In case of equal preemption count, then the local cluster is assigned the job.
    This function reads a log file, where some lines has the job name, and the parameters of each cluster, including the cluster name, start time, and preemption count.
    The function returns a dictionary containing the previous information for each job.
    @param log_file: File path that contains information about each job, including the job name, differernt charactereistics of each cluster sepcified in the "-M/--clusters" options (including cluster name, start time, preemption count)
    @type log_file: str
    @return: Dictionary of extracted information organized by job names as keys, then cluster name, then different characterestics of each cluster
    @rtype: dict
    '''

    res={}  # Final dictionary
    
    with open(log_file,'r') as f:   # Read input log file
        c=f.readlines()
    
    for l in c:
        if l.startswith('job'):
            tmp='{'+l.replace(' ','').replace('job:','job:"').replace(',cluster:','",cluster:"').replace(',start_time','",start_time').replace('job','"job"').replace('cluster:','"cluster":').replace('start_time','"start_time"').replace('preempt_cnt','"preempt_cnt"')+'}'# Preparation of input snetence
            tmp=json.loads(str(tmp))
            res[tmp['job']]={tmp['cluster']:{'start_time':tmp['start_time'],'preempt_cnt':tmp['preempt_cnt']}}
    return res

def extractSqueueData(log_file:str='')->dict:
    ''' Extract data from squeue command. The first line in the log file contains the cluster name and it is not part of the JSON file. Thus, the first line is read seperately, then the remaining dictionary is read as the JSON file
    @param log_file: File path that contains information from squeue
    @type log_file: str
    @return: Dictionary of extracted information
    @rtype: dict
    '''

    with open(log_file,'r') as f:
        cluster=f.readline().strip().replace(" ","").split(':')[-1] # Read the cluster name
        j=json.load(f)  # Read the remaining part as a JSON object
    j['cluster']=cluster    # Add the cluster name to the JSON object
    return j

def extractStatistics(res_file:str='',total_no_cpus:int=1)->dict:
    ''' Extact statistics from an experiement. Collected statistics include: 1) minimum, maximum,
    and avergae of queuing time, elapsed time. 2) total execution time
    @param res_file: Path to a CSV results file
    @type res_file: str
    @total_no_cpus: Total number of CPUs
    @type total_no_cpus: int
    @return: Dictionary of collected statistics
    @rtype: dict
    '''

    dfs=pd.read_csv(res_file)
    min_submission=dfs['submission'].min()  # Minimum submission time to be the base for other timings
    if 'to submission' not in dfs:
        dfs['to submission']=dfs['submission']-min_submission   # Delay time for each job since the first submission
    if 'submissiton to eligible' not in dfs:
        dfs['submission to eligible']=dfs['eligible']-dfs['submission'] # Time to decide eligibility of each job
    if 'eligible to start' not in dfs:
        dfs['eligible to start']=dfs['start']-dfs['eligible']   # Time to start job from eligiblity
    if 'start to end' not in dfs:
        dfs['start to end']=dfs['end']-dfs['start'] # Time to finish job from starting it
    
    total_no_jobs=len(dfs)
    exp_stats={}    # Experiement statistics
    exp_stats['min_elapsed']=dfs['elapsed'].min()
    exp_stats['max_elapsed']=dfs['elapsed'].max()
    exp_stats['avg_elapsed']=dfs['elapsed'].mean()
    exp_stats['total_time']=dfs['end'].max()-dfs['submission'].min()
    exp_stats['submission_to_eligible_min']=dfs['submission to eligible'].min()
    exp_stats['submission_to_eligible_max']=dfs['submission to eligible'].max()
    exp_stats['submission_to_eligible_mean']=dfs['submission to eligible'].mean()
    exp_stats['eligible_to_start_min']=dfs['eligible to start'].min()
    exp_stats['eligible_to_start_max']=dfs['eligible to start'].max()
    exp_stats['eligible_to_start_mean']=dfs['eligible to start'].mean()
    exp_stats['expected jobs iterations']=math.ceil(total_no_jobs/total_no_cpus)
    exp_stats['nodes_util']={}
    for i in dfs['node'].unique():
        exp_stats['nodes_util'][i]=len(dfs.loc[dfs['node']==i])/total_no_jobs
    exp_stats['cluster_util']={}
    for i in dfs['cluster'].unique():
        exp_stats['cluster_util'][i]=len(dfs.loc[dfs['cluster']==i])/total_no_jobs
    
    return exp_stats

def assignEstimatedAvg(res_file:str='',cond:dict={})->None:
    ''' Assign estimated average based on field values
    @param res_file: Path to a CSV results file
    @type res_file: str
    @param cond: Set of conditions to assign estimated average values
    @type cond: dict
    @return: Dictionary of collected statistics
    @rtype: dict
    '''

    #TODO: 



if __name__ == '__main__':
    # Parse input arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('-j', '--json_res_path', help='Path to JSON files resulting from Slurm accounting for finished jobs', default='')  # Path to JSON files resulting from Slurm accounting for finished jobs
    parser.add_argument('-o', '--out_res_path', help='Path of resulting NPB out files', default='')  # Path of resulting NPB out files
    parser.add_argument('-r', '--res_out_path', help='Path and name of final results file (e.g., CSV file) containing extracted information from JSON files', default='final_res_name_sorted.csv')  # Path of final results file (e.g., CSV file) containing extracted information from JSON files
    arguments = parser.parse_args()
    
    if '.json' not in arguments.json_res_path:  # Check the JSON path does not contain names of JSON files
        json_res_path=os.path.join(arguments.json_res_path,"*.json")   # Path of resulting JSON files
    else:
        json_res_path=arguments.json_res_path

    if '.out' not in arguments.out_res_path:    # Check the OUT path does not contain names of OUT files
        out_res_path=os.path.join(arguments.out_res_path,"*.out")    # Path of resulting NPB out files
    else:
        out_res_path=arguments.out_res_path
    
    res_out_path=arguments.res_out_path    # Path of final results file (e.g., CSV file) containing extracted information from JSON files
    
    # Generate CSV table containing all extracted data from workload manager (e.g., Slurm) JSON accouting files, as well NPB output files
    jsonl=glob(json_res_path)    # List of all JSON files under specified path
    outl=glob(out_res_path) # List of all NPB output files under specified path
    json_res_list=[extract_slurm_accounting_res(i) for i in jsonl]    # Extract required information from JSON files in a list of dictionaries
    out_res_list=[extract_npb_results(i) for i in outl] # Extract required information from PNB out files in a list of dictionaries
    json_df=pd.DataFrame.from_dict(json_res_list) # Convert the required JSON information to Panda's DataFrame
    out_df=pd.DataFrame.from_dict(out_res_list) # Convert the required out information into Panda's DataFrame
    final_df=json_df.merge(out_df,on='name')
    final_df=sortJobByName(final_df,res_out_path)

    #final_df.to_csv(res_out_path,index=False,header=True) # Save extracted results in a CSV file

    '''
    #Generate another table with aggreated time values for each iterated experiment (i.e., the same experiement setting repeated for a number of times). Note that each repeatition does not have to run on the same node (and not the same cluster in the case of a multi-cluster experiement):
    agg_final_df=pd.DataFrame({'name':[],'elapsed_avg':[],'elapsed_max':[]}, \
    'elapsed_min':[], 'Time in seconds_avg':[],'Time in seconds_max':[], \
    'Time in seconds_min':[],'Mop/s total_avg':[],'Mop/s toal_max':[],'Mop/s toal_min':[A])

    for setting in range(0,256): # iterate over number of tasks
        final_df_setting=final_df.loc[final_df['name'].str.contains('ed_slurm_srta_'+str(setting)+'_'),['elapsed','Time in seconds','Mop/s total']] # To extract only iterated records for the same exeriement. Thus, aggregated statistics (e.g., average, max, min) can be done

    df3=final_df[final_df['name'].str.contains('ed_slurm_srta_0_')] # To extract only iterated records for the same exeriement. Thus, averging time can be done
    df4=df3.loc[:,df3.columns!='node']  # Exclude 'node' column
    df4=df3.drop(['node','cluster'],axis=1) # Another way to remove the 'node' and 'cluster' columns
    '''

    ############# Curvefitting to derive linear model of cluster(s) #################
    p=clusterCurveFitting(final_res='final_res.csv',md_fn=md)
    r_inf=p['r_inf']    
    n_half=p['n_half']

    ############### Draw the linear model with scatter data ################
    #TODO
    plt.plot(x=sorted_ends,y=[md(i,r_inf,n_half) for i in sorted_ends],label="r_inf intrusive")
    plt.scatter(x=sorted_ends,y=jobs,c='red',marker='+',label='No. job per time interval')
    plt.legend(loc='best')
    plt.xlabel("Time interval in Sec")
    plt.ylabel("No. jobs")
    plt.savefig("testf4.jpeg")
    
    ################## Extract squeue information ##################
    #TODO: Collecting JSON files needs modifications to be more general
    s2_sq_files=['squeue_slurmcluster2_ed_slurm_srta_all_'+str(i)+'_0_1698954689_780109933.json' for i in range(144)]
    asu_sq_files=['squeue_asuslurm_ed_slurm_srta_all_'+str(i)+'_0_1698954689_780109933.json' for i in range(144)]
    df1=pd.read_csv('final_res_name_sort.csv')

    sq_ext_data=[] # Hold extracted data from JSON files
    d={} # Temporary dictionary
    for i in range(144):
        d['slurmcluster2']=extractSqueueData(s2_sq_files[i])
        d['asuslurm']=extractSqueueData(asu_sq_files[i])
        sq_ext_data.append(d)
    
    t={}
    for i in sq_ext_data[0]['slurmcluster2']['jobs']:
        t['name']=i['name']
        t['cluster']=i['cluster']
        t['accrue_time']=i['accrue_time']['number']
        t['start_time']=i['start_time']['number']
        t['cpus']=i['cpus']['number']
        t['end_time']=i['end_time']['number']
        t['eligible_time']=i['eligible_time']['number']
        t['last_sched_evaluation']=i['last_sched_evaluation']['number']
        t['submit_time']=i['submit_time']['number']
        print(t)
        print()

    ################ SORT LOCAL BENCHMARK FILES BY NAME #####################
    dfs1=dfs[0:0]
    exp_timestamp="1700369335_183859789"
    for i in range(10,150,10):
        for j in range(i):
            n="ed_slurm_srta_all_local_profile_"+str(j)+"_"+str(i)+exp_timestamp
            k=dfs.loc[dfs['name']==n]
            dfs1=dfs1.append(k)
    dfs1.to_csv('final_res_name.csv')

    ############### EXTRACT THROUGHPUT FOR NAME_SORTED FILES ##############
    dfs=pd.read_csv('final_res_name_sort.csv')
    min_submission=dfs['submission'].min()
    dfs['throughput_no_jobs']=dfs['end'].apply(lambda x:(dfs['end']<=x).sum())
    dfs['throughput']=dfs['throughput_no_jobs']/(dfs['end']-min_submission)
    dfs.to_csv('final_res_name_sort.csv')
    plt.clf()
    fig=dfs.plot.bar(x='node',y='throughput',rot=90,fontsize=8)
    fig.figure.set_figwidth(35)
    fig.figure.set_figheight(10)
    fig.figure.savefig('throughput_name_sort.jpeg')
    #fig.figure.show()

    ############### SORT FILES BASED ON END TIME, THEN INDEX (ORDER TO SUBMIT JOBS) ##############
    dfs=pd.read_csv('final_res_name_sort.csv')
    dfs['cnt']=dfs.index+1  # Corresponding column to job index
    dfs2=dfs.sort_values(['end','cnt']) # Sort file by end time first,then ties broken by job index
    dfs2.to_csv('final_res_end_index_sort.csv') # Save newly sorted file

    ############### EXTRACT THROUGHPUT FOR END_INDEX_SORTED FILES ##############
    dfs2=pd.read_csv('final_res_end_index_sort.csv')
    min_submission=dfs2['submission'].min()
    dfs2['throughput_no_jobs']=dfs2['end'].apply(lambda x:(dfs2['end']<=x).sum())
    dfs2['throughput']=dfs2['throughput_no_jobs']/(dfs2['end']-min_submission)
    dfs2.to_csv('final_res_end_index_sort.csv') # Save newly sorted file
    plt.clf()
    fig=dfs2.plot.bar(x='node',y='throughput',rot=90,fontsize=8)
    fig.figure.set_figwidth(35)
    fig.figure.set_figheight(10)
    fig.figure.savefig('throughput_end_index_sort.jpeg')
    #fig.figure.show()

    ############## CALCULATED ESTIMATED NUMBER OF JOBS FOR EACH END TIME ##############
    srta_avg=58.5   # Average execution time of sp.A jobs on SRTA based on experimental benchmarking
    asu_avg=248.5575    # Average execution time of sp.A jobs on ASU based on experimental benchmarking
    srta_cpus=32    # Number of CPUs in SRTA cluster
    asu_cpus=32     # Number of CPUs in ASU cluster
    max_no_jobs=144 # Maximum number of submitted jobs
    dfs2=pd.read_csv('final_res_end_index_sort.csv')
    min_submission=dfs2['submission'].min()
    clusters=dfs2['cluster'].unique()    # Clusters of current experiement
    if clusters.size>1: 
        # Both SRTA and ASU are used
        dfs2['est_throughput_no_jobs']=(dfs2['end']-min_submission)*(srta_cpus/srta_avg+asu_cpus/asu_avg)
    elif clusters[0]=='slurmcluster2':
        # Only SRTA is used
        dfs2['est_throughput_no_jobs']=(dfs2['end']-min_submission)*(srta_cpus/srta_avg)
    else:
        # Only ASU is used
        dfs2['est_throughput_no_jobs']=(dfs2['end']-min_submission)*(asu_cpus/asu_avg)


    dfs2['est_throughput_no_jobs']=dfs2['est_throughput_no_jobs'].apply(lambda x:max_no_jobs if x>max_no_jobs else x)   # Set expected finished number of jobs to maximum number if expected value exceeds it
    dfs2['est_throughput']=dfs2['est_throughput_no_jobs']/(dfs2['end']-min_submission)
    dfs2.to_csv('final_res_end_index_sort.csv')
    plt.clf()
    fig=dfs2.plot.line(x='end',y=['est_throughput','throughput'],fontsize=8,rot=90,legend=True)
    fig.figure.set_figwidth(35)
    fig.figure.set_figheight(10)
    fig.figure.savefig('estimated vs measured throughput per time interval since first submission for jobs ordered by end time, then index.jpeg')
    plt.clf()
    fig=dfs2.plot.line(x='end',y=['est_throughput_no_jobs','throughput_no_jobs'],fontsize=8,rot=90,legend=True)
    fig.figure.set_figwidth(35)
    fig.figure.set_figheight(10)
    fig.figure.savefig('estimated vs measured number of finished jobs per time interval since first submission for jobs ordered by end time, then index.jpeg')
    # Plot throughput and total time vs number of jobs instead of time
    plt.clf()
    dfs2['total_time (sec)']=dfs2['end']-min_submission
    dfs2['no_jobs']=dfs2['throughput_no_jobs']
    fig=dfs2.plot.line(x='no_jobs',y='throughput',fontsize=8,rot=90,legend=True)
    fig.figure.set_figwidth(35)
    fig.figure.set_figheight(10)
    fig.figure.savefig('no_jobs vs throughput.jpeg')
    plt.clf()
    fig=dfs2.plot.line(x='no_jobs',y='total_time (sec)',fontsize=8,rot=90,legend=True)
    fig.figure.set_figwidth(35)
    fig.figure.set_figheight(10)
    fig.figure.savefig('no_jobs vs total_time (sec).jpeg')

    ####### PREPARE RESULTS FROM PBS (E.G., CREATE REQUIRED FIELDS LIKE THROUGHPUT AND THROUGHPUT NUMBER AFTER REORDERING DATA) FROM EXCEL FILES ########
    pbs_excel_file='ASU-SRTA delay1.xlsx'
    pbs_excel_file_sorted='asu_srta_delay1_sort_end_index.csv'
    dfs=pd.read_excel(pbs_excel_file)
    # Get rid of white spaces in time columns
    dfs['Creation Time']=dfs['Creation Time'].str.strip()
    dfs['Queue Time']=dfs['Queue Time'].str.strip()
    dfs['Start Time']=dfs['Start Time'].str.strip()
    dfs['Wall Time']=dfs['Wall Time'].str.strip()

    # Convert and create time columns to datetime and timedelta objects
    dfs['creation_time_abs']=pd.to_datetime(dfs['Creation Time'],format='%H:%M:%S')
    dfs['queue_time_abs']=pd.to_datetime(dfs['Queue Time'],format='%H:%M:%S')
    dfs['start_time_abs']=pd.to_datetime(dfs['Start Time'],format='%H:%M:%S')
    dfs['wall_time_sec']=pd.to_timedelta(dfs['Wall Time'])/1E9  # Convert to timedelta of seconds instead of nanoseconds, despite the type of the column is still nsec
    dfs['queue_time_len_sec']=(dfs['start_time_abs']-dfs['creation_time_abs'])/1E9    # Total job time in seconds
    dfs['end_time_abs']=dfs['start_time_abs']+dfs['wall_time_sec']*1E9
    dfs['cnt']=dfs.index+1
    dfs.to_excel(pbs_excel_file)    # Save the file again with the new added columns

    # Create the new excel file sorted by end time, then job index. Add throughput calculations to the newly created file
    dfs2=dfs.sort_values(['end_time_abs','cnt'])
    min_submission=dfs2['creation_time_abs'].min()
    dfs2['throughput_no_jobs']=dfs2['end_time_abs'].apply(lambda x:(dfs2['end_time_abs']<=x).sum())
    dfs2['throughput']=dfs2['throughput_no_jobs']/(dfs2['end_time_abs']-min_submission).astype('int64')*1E9
    dfs2.to_csv(pbs_excel_file_sorted)

    






