#!/bin/sh
#SBATCH --account=all_clusters_account
#SBATCH --time=30

echo "start time before srun: "$(date +%s.%N)
srun bash $HOME/test_srun.sh 3
echo "end time after srun: "$(date +%s.%N)
