#!/bin/bash
#SBATCH --time=30 

export SLURM_EXPORT_ENV="ALL" # This line shoule be called with sbatch --export=NONE to allow executing the script on another cluster with different user environment. For more information, refer to the --export option in the sbatch command

#srun --mpi=pmi2 /bin/echo $exec_script, $res_dir, $res_prefix, $i, $rep, $exp_time
srun --mpi=pmi2 -o $res_dir/$exp_time/$res_prefix\_$i\_$rep\_$exp_time.out -e $res_dir/$exp_time/$res_prefix\_$i\_$rep\_$exp_time.err $exec_script
