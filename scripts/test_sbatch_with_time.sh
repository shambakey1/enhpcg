#!/bin/bash

IFS='= ' read _ local_clusterid <<< $(scontrol show config |grep -i clustername) # Extract local cluster name
echo "Local cluster: "$local_clusterid

# Check input clusters to submit job(s)
if [ -z $1 ]; then
    clusterid=$local_clusterid
else
    clusterid=$1
fi

# Check input constraints
if [ -z $2 ]; then
    cons=''
else
    cons=$2
fi

# Check input number of tasks
if [ -z $3 ]; then
    no_tasks=1
else
    no_tasks=$3
fi

echo "start time before sbatch "$(date +%s.%N)
job_cluster_id=$(sbatch --parsable --wait -M$clusterid --constraint=$cons -n$no_tasks test_srun_with_time.sh) # Since sbatch is a non-blocking operation, --wait is used to block sbatch until response is received. Input arguments are cluster(s) name(s), constraints, number of tasks
echo "end time after sbatch "$(date +%s.%N)
IFS=';' read jobid clusterid <<< $job_cluster_id # Extract job ID and cluster ID (if not local cluster)
echo "JOB ID: "$jobid", clusterid: "$clusterid
sacct -j $jobid -M$clusterid --json > "test_"$jobid"_cluster_"$clusterid".json" # Extract job statistics into a JSON file