#!/bin/bash

max_njobs=144 # Max number of jobs. Minimum is 0
max_rep=1 # Maximum number of repeatition for each experiement.
res_dir="/home/test_user3/NPB3.3.1/NPB3.3-SER/ed_results/htcondor/144serial/" # Path to keep results
res_prefix="ed_htcondor_srta" # Prefix for output/error files
exp_time=$(date +%s_%N) # Initiation time of current experiments. Used as UUID for resulting files
#clusters="slurmcluster2" # List of clusters to submit slurm jobs
mkdir -p $res_dir/$exp_time # Create sub-directory to store results of current experiment
#res_dir_timed=$res_dir/$exp_time # Modify path to store results of current experiment by appending current time

for (( rep=0; rep<$max_rep; rep++ )) # Traverse over required number of repeatitions
do
        # Submit jobs for execution on specified clusters
        echo "Submitting jobs for repitition $rep"
        for (( i=0; i<$max_njobs; i++ ))
        do

                condor_submit -batch-name $res_prefix\_$i\_$rep\_$exp_time log=$res_dir/$exp_time/$res_prefix\_$i\_$rep\_$exp_time.log output=$res_dir/$exp_time/$res_prefix\_$i\_$rep\_$exp_time.out error=$res_dir/$exp_time/$res_prefix\_$i\_$rep\_$exp_time.err executable=$exec_script serial144_srta_htcondor.sub
                sleep 1 # Wait for some time before submitting the next job
        done

        # Collect statistics of jobs in JSON files
        for (( i=0; i<$max_njobs; i++ )) # Traverse over required number of jobs
        do
                echo "Statistics about job: $res_prefix\_$i\_$rep\_$exp_time"

                condor_wait $res_dir/$exp_time/$res_prefix\_$i\_$rep\_$exp_time.log     # Wait until specified job finishes
                condor_history -json -constraint "JobBatchName==\""$res_prefix"_"$i"_"$rep"_"$exp_time"\"" > $res_dir/$exp_time/$res_prefix\_$i\_$rep\_$exp_time.json
        done

        # Copy any files that exist on the other cluster to current results directory
        #scp -r asuslrhd:/hpcslurmd/NPB3.3.1/NPB3.3-SER/ed_results/slurm/$exp_time $res_dir
done
