#!/bin/bash

max_njobs=256 # Max number of jobs. Minimum is 0
max_rep=3 # Maximum number of repeatition for each experiement.
res_dir="/home/test_user3/NPB3.3.1/NPB3.3-SER/ed_results/slurm" # Path to keep results
res_prefix="ed_slurm_srta" # Prefix for output/error files
exp_time=$(date +%s_%N) # Initiation time of current experiments. Used as UUID for resulting files
clusters="slurmcluster2" # List of clusters to submit slurm jobs
mkdir -p $res_dir/$exp_time # Create sub-directory to store results of current experiment
res_dir+=/$exp_time # Modify path to store results of current experiment by appending current time

for (( rep=0; rep<$max_rep; rep++ )) # Traverse over required number of repeatitions
do
        echo "Submitting jobs for repitition $rep"
        for (( i=0; i<$max_njobs; i++ ))
        do
                srun -n1 -N1 --mem=70mb --job-name=$res_prefix\_$i\_$rep\_$exp_time -o $res_dir/$res_prefix\_$i\_$rep\_$exp_time.out -e $res_dir/$res_prefix\_$i\_$rep\_$exp_time.err -A all_clusters_account -M$clusters /home/test_user3/NPB3.3.1/NPB3.3-SER/bin/sp.A.x &
        done
        wait

        # Collect statistics of jobs in JSON files
        for (( i=0; i<$max_njobs; i++ )) # Traverse over required number of jobs
        do
                echo "Statistics about job: $res_prefix\_$i\_$rep\_$exp_time"
                sacct --name=$res_prefix\_$i\_$rep\_$exp_time -M$clusters --json > $res_dir/$res_prefix\_$i\_$rep\_$exp_time.json
        done
done
